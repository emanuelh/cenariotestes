package emanuelh.cenario_teste;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Cenario {
	
	@Test
	public void testarCenario() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\lucia\\OneDrive\\Área de Trabalho\\trainner\\drives _de_browser\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("file:///" + System.getProperty("user.dir") + "/componentes.html");
		driver.manage().window().maximize();
		
		WebElement btnCadastrar = driver.findElement(By.id("elementosForm:cadastrar"));
		WebElement inputNome = driver.findElement(By.id("elementosForm:nome"));
		WebElement inputSobrenome = driver.findElement(By.id("elementosForm:sobrenome"));
		
		btnCadastrar.click();
		Alert alerta = driver.switchTo().alert();
		String msgAlerta = alerta.getText();
		alerta.accept();
		Assert.assertEquals("Nome eh obrigatorio", msgAlerta);
		
		inputNome.sendKeys("Emanuel");
		btnCadastrar.click();
		alerta = driver.switchTo().alert();
		msgAlerta = alerta.getText();
		alerta.accept();
		Assert.assertEquals("Sobrenome eh obrigatorio", msgAlerta);
		
		inputSobrenome.sendKeys("Honorio");
		btnCadastrar.click();
		alerta = driver.switchTo().alert();
		msgAlerta = alerta.getText();
		alerta.accept();
		Assert.assertEquals("Sexo eh obrigatorio", msgAlerta);
		
		WebElement sexo = driver.findElement(By.id("elementosForm:sexo"));
		sexo.click();
		
		WebElement comidaFavorita = driver.findElement(By.id("elementosForm:comidaFavorita:1"));
		comidaFavorita.click();
		
		Select selectEscolaridade = new Select(driver.findElement(By.id("elementosForm:escolaridade")));
		selectEscolaridade.selectByValue("2graucomp");
		
		Select selectEsportes = new Select(driver.findElement(By.id("elementosForm:esportes")));
		selectEsportes.selectByValue("futebol");
		
		WebElement inputSugestoes = driver.findElement(By.id("elementosForm:sugestoes"));
		inputSugestoes.sendKeys("Colocar eu na equipe de testes");
		
		btnCadastrar.click();
		
		WebElement statusCadastrado = driver.findElement(By.xpath("//*[@id=\"resultado\"]/span"));
		String txtStatusCadastrado = statusCadastrado.getText();
		Assert.assertEquals("Cadastrado!", txtStatusCadastrado);
		
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.quit();
	}
}
